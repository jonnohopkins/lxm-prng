#include <gtest/gtest.h>
#include <lxm/lxm.h>
#include <sstream>

using namespace lxm;

static_assert(std::uniform_random_bit_generator<l64x128m>);

static std::random_device g_rd;

// todo: should really be using something like PractRand to test the properties of the random number generator.
// https://pracrand.sourceforge.net/
// without any such tests you probably shouldn't trust this library.

TEST(lxm, stream_ops)
{
	lxm::l64x128m	  lxm_engine(g_rd());
	std::stringstream ss;

	for (auto i = 0; i < 1024; ++i) {
		ss << lxm_engine;
		
		lxm::l64x128m reconstructed;
		ss >> reconstructed;

		EXPECT_EQ(lxm_engine, reconstructed);

		lxm_engine.discard(1);
		ss.str("");
		ss.clear();
	}
}
