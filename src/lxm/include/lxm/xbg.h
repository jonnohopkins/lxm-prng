#pragma once

#include <cstdint>
#include <limits>
#include <utility>
#include <bit>
#include <ostream>
#include <istream>
#include <array>
#include <lxm/random.h>

namespace lxm {

class xoroshiro64_engine {
  public:
	using result_type = std::uint_fast32_t;

	static constexpr result_type default_seed = 0xaa801adeul;

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	explicit xoroshiro64_engine(result_type s = default_seed) { seed(s); }
	template<typename Sseq>
	explicit xoroshiro64_engine(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoroshiro64_engine>
	{
		seed(std::forward<Sseq>(sseq));
	}

	xoroshiro64_engine(const xoroshiro64_engine&)			 = default;
	xoroshiro64_engine(xoroshiro64_engine&&)				 = default;
	xoroshiro64_engine& operator=(const xoroshiro64_engine&) = default;
	xoroshiro64_engine& operator=(xoroshiro64_engine&&)		 = default;

	void seed(result_type s = default_seed) { seed(std::seed_seq{ s }); }
	template<typename Sseq>
	void seed(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoroshiro64_engine>
	{
		std::array<std::uint_least32_t, 2> seeds;
		sseq.generate(seeds.begin(), seeds.end());
		x0 = seeds[0];
		x1 = seeds[1];

		if (x0 == 0 && x1 == 0)
			throw prng_state_error{ "random number engine state must be non-zero." };
	}

	// Figure 1 and Table 5 from the paper: Scrambled Linear Pseudorandom Number Generators
	result_type operator()()
	{
		result_type q0 = x0, q1 = x1;
		q1 ^= q0;
		x0 = std::rotl(q0, 26) ^ q1 ^ (q1 << 9);
		x1 = std::rotl(q1, 13);
		return x0;
	}

	void discard(unsigned long long z)
	{
		for (unsigned long long i = 0; i < z; ++i)
			operator()();
	}

	friend bool			 operator==(const xoroshiro64_engine&, const xoroshiro64_engine&) = default;
	friend std::ostream& operator<<(std::ostream& os, const xoroshiro64_engine& rhs) { return os << rhs.x0 << ' ' << rhs.x1; }
	friend std::istream& operator>>(std::istream& is, xoroshiro64_engine& rhs)
	{
		is >> rhs.x0;
		is >> rhs.x1;
		return is;
	}

  private:
	result_type x0, x1; // x0 and x1 are never both zero
};

class xoroshiro128_engine {
  public:
	using result_type = std::uint_fast64_t;

	static constexpr result_type default_seed = 0x6896c17e1f5a6934ull;

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	explicit xoroshiro128_engine(result_type s = default_seed) { seed(s); }
	template<typename Sseq>
	explicit xoroshiro128_engine(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoroshiro128_engine>
	{
		seed(std::forward<Sseq>(sseq));
	}

	xoroshiro128_engine(const xoroshiro128_engine&)			   = default;
	xoroshiro128_engine(xoroshiro128_engine&&)				   = default;
	xoroshiro128_engine& operator=(const xoroshiro128_engine&) = default;
	xoroshiro128_engine& operator=(xoroshiro128_engine&&)	   = default;

	void seed(result_type s = default_seed) { seed(std::seed_seq{ s }); }
	template<typename Sseq>
	void seed(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoroshiro128_engine>
	{
		std::array<std::uint_least32_t, 4> seeds;
		sseq.generate(seeds.begin(), seeds.end());
		x0 = detail::combine(seeds[0], seeds[1]);
		x1 = detail::combine(seeds[2], seeds[3]);

		if (x0 == 0 && x1 == 0)
			throw prng_state_error{ "random number engine state must be non-zero." };
	}

	// Figure 1 and Table 3 from the paper: Scrambled Linear Pseudorandom Number Generators
	result_type operator()()
	{
		result_type q0 = x0, q1 = x1;
		q1 ^= q0;
		x0 = std::rotl(q0, 24) ^ q1 ^ (q1 << 16);
		x1 = std::rotl(q1, 37);
		return x0;
	}

	void discard(unsigned long long z)
	{
		for (unsigned long long i = 0; i < z; ++i)
			operator()();
	}

	friend bool			 operator==(const xoroshiro128_engine&, const xoroshiro128_engine&) = default;
	friend std::ostream& operator<<(std::ostream& os, const xoroshiro128_engine& rhs) { return os << rhs.x0 << ' ' << rhs.x1; }
	friend std::istream& operator>>(std::istream& is, xoroshiro128_engine& rhs)
	{
		is >> rhs.x0;
		is >> rhs.x1;
		return is;
	}

  private:
	result_type x0, x1; // x0 and x1 are never both zero
};

class xoshiro256_engine {
  public:
	using result_type = std::uint_fast64_t;

	static constexpr result_type default_seed = 0x2b77a1259a5950c2ull;

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	explicit xoshiro256_engine(result_type s = default_seed) { seed(s); }
	template<typename Sseq>
	explicit xoshiro256_engine(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoshiro256_engine>
	{
		seed(std::forward<Sseq>(sseq));
	}

	xoshiro256_engine(const xoshiro256_engine&)			   = default;
	xoshiro256_engine(xoshiro256_engine&&)				   = default;
	xoshiro256_engine& operator=(const xoshiro256_engine&) = default;
	xoshiro256_engine& operator=(xoshiro256_engine&&)	   = default;

	void seed(result_type s = default_seed) { seed(std::seed_seq{ s }); }
	template<typename Sseq>
	void seed(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoshiro256_engine>
	{
		std::array<std::uint_least32_t, 8> seeds;
		sseq.generate(seeds.begin(), seeds.end());
		x0 = detail::combine(seeds[0], seeds[1]);
		x1 = detail::combine(seeds[2], seeds[3]);
		x2 = detail::combine(seeds[4], seeds[5]);
		x3 = detail::combine(seeds[6], seeds[7]);

		if (x0 == 0 && x1 == 0 && x2 == 0 && x3 == 0)
			throw prng_state_error{ "random number engine state must be non-zero." };
	}

	// Figure 4 and Table 3 from paper: Scrambled Linear Pseudorandom Number Generators
	result_type operator()()
	{
		result_type q0 = x0, q1 = x1, q2 = x2, q3 = x3;
		result_type t = q1 << 17;
		q2 ^= q0;
		q3 ^= q1;
		q1 ^= q2;
		q0 ^= q3;
		q2 ^= t;
		q3 = std::rotl(q3, 45);
		x0 = q0;
		x1 = q1;
		x2 = q2;
		x3 = q3;
		return x0;
	}

	void discard(unsigned long long z)
	{
		for (unsigned long long i = 0; i < z; ++i)
			operator()();
	}

	friend bool			 operator==(const xoshiro256_engine&, const xoshiro256_engine&) = default;
	friend std::ostream& operator<<(std::ostream& os, const xoshiro256_engine& rhs) { return os << rhs.x0 << ' ' << rhs.x1 << ' ' << rhs.x2 << ' ' << rhs.x3; }
	friend std::istream& operator>>(std::istream& is, xoshiro256_engine& rhs)
	{
		is >> rhs.x0;
		is >> rhs.x1;
		is >> rhs.x2;
		is >> rhs.x3;
		return is;
	}

  private:
	result_type x0, x1, x2, x3; // x0, x1, x2, x3 must not all be 0
};

class xoroshiro1024_engine {
  public:
	using result_type = std::uint_fast64_t;

	static constexpr result_type default_seed = 0xef8fbb0555a95775ull;

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	explicit xoroshiro1024_engine(result_type s = default_seed) { seed(s); }
	template<typename Sseq>
	explicit xoroshiro1024_engine(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoroshiro1024_engine>
	{
		seed(std::forward<Sseq>(sseq));
	}

	xoroshiro1024_engine(const xoroshiro1024_engine&)			 = default;
	xoroshiro1024_engine(xoroshiro1024_engine&&)				 = default;
	xoroshiro1024_engine& operator=(const xoroshiro1024_engine&) = default;
	xoroshiro1024_engine& operator=(xoroshiro1024_engine&&)		 = default;

	void seed(result_type s = default_seed) { seed(std::seed_seq{ s }); }
	template<typename Sseq>
	void seed(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, xoroshiro1024_engine>
	{
		std::array<std::uint_least32_t, 32> seeds;
		sseq.generate(seeds.begin(), seeds.end());
		for (size_t i = 0; i < 16; ++i)
			x[i] = detail::combine(seeds[i * 2], seeds[i * 2 + 1]);

		if (std::all_of(x.begin(), x.end(), [](result_type v) { return v == 0; }))
			throw prng_state_error{ "random number engine state must be non-zero." };
	}

	// Figure 2 and Table 3 from paper: Scrambled Linear Pseudorandom Number Generators
	result_type operator()()
	{
		const int q		   = p;
		p				   = (p + 1) & 15;
		const uint64_t s0  = x[p];
		uint64_t	   s15 = x[static_cast<size_t>(q)];
		s15 ^= s0;
		x[static_cast<size_t>(q)] = std::rotl(s0, 25) ^ s15 ^ (s15 << 27);
		x[p] = std::rotl(s15, 36);

		return x[0];
	}

	void discard(unsigned long long z)
	{
		for (unsigned long long i = 0; i < z; ++i)
			operator()();
	}

	friend bool			 operator==(const xoroshiro1024_engine&, const xoroshiro1024_engine&) = default;
	friend std::ostream& operator<<(std::ostream& os, const xoroshiro1024_engine& rhs)
	{
		for (size_t i = 0; i < rhs.x.size() - 1; ++i)
			os << rhs.x[i] << ' ';
		os << rhs.x.back();
		return os;
	}
	friend std::istream& operator>>(std::istream& is, xoroshiro1024_engine& rhs)
	{
		for (size_t i = 0; i < rhs.x.size(); ++i)
			is >> rhs.x[i];
		return is;
	}

  private:
	std::array<result_type, 16> x; // elements must not all be 0
	uint16_t					p; // index in the range [0, 16)
};

} // namespace lxm
