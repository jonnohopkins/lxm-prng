#pragma once

#include <exception>
#include <concepts>
#include <type_traits>
#include <cstdint>
#include <random>

namespace lxm {
namespace detail {
template<class SeedSeq, class Engine>
concept seed_sequence = !std::convertible_to<std::remove_cvref_t<SeedSeq>, typename Engine::result_type> && !std::same_as<std::remove_cvref_t<SeedSeq>, Engine>;

inline std::uint_fast64_t combine(std::uint_least32_t a, std::uint_least32_t b) { return (static_cast<std::uint_fast64_t>(a) << 32) + b; };
} // namespace detail

struct prng_state_error : public std::runtime_error {
	explicit prng_state_error(const std::string& msg) : std::runtime_error(msg.c_str()) {}
	explicit prng_state_error(const char* msg) : std::runtime_error(msg) {}
};

} // namespace lxm
