#pragma once

#include <limits>
#include <ostream>
#include <istream>
#include <array>
#include <lxm/random.h>
#include <lxm/lcg.h>
#include <lxm/xbg.h>
#include <lxm/mixer.h>

namespace lxm {

/**
* A split() operation returns an (ideally) statistically independent random number engine from the original object (which is modified as a result of this call).
*/
// template<typename Rand>
// concept splittable_random_number_engine = RandomNumberEngine-named-requirement<Rand> && std::copy_constructible<Rand> && requires(Rand& rand) {
//																										  {
//																											  rand.split()
//																											  } -> std::same_as<Rand>;
//																									  };

#if defined(LXM_DEBUG) && LXM_DEBUG == 1
// see splittable_random_number_engine concept definition.
struct splittable_error : public std::logic_error {
	explicit splittable_error(const std::string& msg) : std::logic_error(msg.c_str()) {}
	explicit splittable_error(const char* msg) : std::logic_error(msg) {}
};
#endif

template<typename Combine, typename LcgResult, typename XbgResult>
concept combiner = std::semiregular<Combine> && std::unsigned_integral<LcgResult> && std::unsigned_integral<XbgResult> &&
	std::regular_invocable<const Combine&, LcgResult, XbgResult> && std::unsigned_integral<std::invoke_result_t<const Combine&, LcgResult, XbgResult>>;

template<typename Mix, typename UInt>
concept mixer = std::semiregular<Mix> && std::unsigned_integral<UInt> && std::regular_invocable<const Mix&, UInt> &&
	std::same_as<std::invoke_result_t<const Mix&, UInt>, UInt>;

// A member of the LXM family of algorithms for word size w (where w is any non-negative integer, but typically either 64 or 32) consists of four
// components:
//  - L: a linear congruential pseudorandom number generator (LCG) with a k-bit state 's', k >= w
//  - X: an F2-linear [L'Ecuyer and Panneton 2009] pseudorandom number generator (we use the term XBG, for "xor-based generator")
// with an n-bit state 'x', n >= w
//  - a simple combining operation on two w-bit operands that produces a w-bit result (typically std::plus or std::bit_xor)
//  - M: a bijective mixing function that maps a w-bit argument to a w-bit result
template<std::uniform_random_bit_generator /* RandomNumberEngine named requirement */ LCG,
	std::uniform_random_bit_generator /* RandomNumberEngine named requirement */ XBG, combiner<typename LCG::result_type, typename XBG::result_type> Combine,
	mixer<std::invoke_result_t<const Combine&, typename LCG::result_type, typename XBG::result_type>> Mix>
	requires((sizeof(typename LCG::result_type) >= sizeof(std::uint_fast64_t)) &&
		std::same_as<std::invoke_result_t<const Mix&, std::invoke_result_t<const Combine&, typename LCG::result_type, typename XBG::result_type>>,
			std::uint_fast64_t>)
class lxm_engine {
  private:
	// the (minimum) number of elements generated (by a call to s.generate()) from the named requirement SeedSequence when this RNE is constructed from a
	// SeedSequence 's'.
	template<typename RNE> // RNE models the named requirement RandomNumberEngine.
	static constexpr size_t required_seed_seq_length = sizeof(RNE) / sizeof(std::uint_least32_t);

  public:
	using result_type  = std::uint_fast64_t;
	using lcg_type	   = LCG;
	using xbg_type	   = XBG;
	using combine_type = Combine;
	using mix_type	   = Mix;

	static constexpr size_t		 word_size	  = sizeof(result_type) * CHAR_BIT;
	static constexpr result_type default_seed = 0xe140b20c31e5c7a2ull;

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	explicit lxm_engine(result_type s = default_seed) { seed(s); }
	template<typename Sseq>
	explicit lxm_engine(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, lxm_engine>
	{
		seed(std::forward<Sseq>(sseq));
	}

	lxm_engine(const lxm_engine&)			 = default;
	lxm_engine(lxm_engine&&)				 = default;
	lxm_engine& operator=(const lxm_engine&) = default;
	lxm_engine& operator=(lxm_engine&&)		 = default;

	void seed(result_type s = default_seed) { seed(std::seed_seq{ s }); }
	template<typename Sseq>
	void seed(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, lxm_engine>
	{
		static constexpr size_t total_required_seed_seq_length = required_seed_seq_length<lcg_type> + required_seed_seq_length<xbg_type>;
		std::array<std::uint_least32_t, total_required_seed_seq_length> seeds;
		sseq.generate(seeds.begin(), seeds.end());

		m_lcg.seed(std::seed_seq{ seeds.begin(), seeds.begin() + required_seed_seq_length<lcg_type> });
		try {
			m_xbg.seed(std::seed_seq{ seeds.begin() + required_seed_seq_length<lcg_type>, seeds.end() });
		}
		catch (const prng_state_error&) {
			// use m_lcg to seed m_xbg instead:
			bool																seed_success = false;
			std::array<std::uint_least32_t, required_seed_seq_length<xbg_type>> xbg_seeds;

			do {
				create_seeds_from_prng(xbg_seeds, m_lcg);
				std::seed_seq xbg_sseq{ xbg_seeds.begin(), xbg_seeds.end() };

				try {
					m_xbg.seed(std::move(xbg_sseq));
					seed_success = true;
				}
				catch (const prng_state_error&) {
				}
			} while (!seed_success);
		}
	}

	lxm_engine split()
	{
#if defined(LXM_DEBUG) && LXM_DEBUG == 1
		if (m_has_generated)
			throw splittable_error{ "random number engine has previously generated a value." };
#endif

		static constexpr size_t total_required_seed_seq_length = required_seed_seq_length<lcg_type> + required_seed_seq_length<xbg_type>;
		std::array<std::uint_least32_t, total_required_seed_seq_length> seeds;
		create_seeds_from_prng(seeds, *this);

#if defined(LXM_DEBUG) && LXM_DEBUG == 1
		// create_seeds_from_prng(seeds, *this) will generate values from this object, but we consider that to be part of the split operation.
		// therefore, suppress validation failure.
		m_has_generated = false;
#endif

		std::seed_seq sseq{ seeds.begin(), seeds.end() };
		return lxm_engine{ std::move(sseq) };
	}

	result_type operator()()
	{
#if defined(LXM_DEBUG) && LXM_DEBUG == 1
		m_has_generated = true;
#endif

		return m_mix(m_combine(m_lcg(), m_xbg()));
	}

	void discard(unsigned long long z)
	{
#if defined(LXM_DEBUG) && LXM_DEBUG == 1
		m_has_generated = true;
#endif

		// no need to combine or mix the sub-generators, simply discard the sub-generators.
		m_lcg.discard(z);
		m_xbg.discard(z);
	}

	friend bool operator==(const lxm_engine& lhs, const lxm_engine& rhs) { return lhs.m_lcg == rhs.m_lcg && lhs.m_xbg == rhs.m_xbg; }

	// does not write debug state to the stream (to keep a consistent binary format when debug functionality is enabled/disabled).
	// therefore when reading from a stream we conservatively assume that it was used to generate a value.
	friend std::ostream& operator<<(std::ostream& os, const lxm_engine& rhs) { return os << rhs.m_lcg << ' ' << rhs.m_xbg; }
	friend std::istream& operator>>(std::istream& is, lxm_engine& rhs)
	{
		is >> rhs.m_lcg;
		is >> rhs.m_xbg;
#if defined(LXM_DEBUG) && LXM_DEBUG == 1
		rhs.m_has_generated = true;
#endif
		return is;
	}

  private:
	template<size_t size, std::uniform_random_bit_generator RNG> static void create_seeds_from_prng(std::array<std::uint_least32_t, size>& seeds, RNG& rng)
	{
		for (size_t i = 0; i < size / 2; ++i) {
			result_type rand = rng();
			seeds[i * 2]	 = static_cast<std::uint_least32_t>(rand); // low 32 bits.
			seeds[i * 2 + 1] = static_cast<std::uint_least32_t>(rand >> 32); // high 32 bits;
		}
		if constexpr (size % 2 != 0) {
			result_type rand = rng();
			seeds[size - 1]	 = static_cast<std::uint_least32_t>(rand); // low 32 bits.
		}
	}

	lcg_type	 m_lcg;
	xbg_type	 m_xbg;
	combine_type m_combine;
	mix_type	 m_mix;
#if defined(LXM_DEBUG) && LXM_DEBUG == 1
	bool m_has_generated{ false };
#endif
};

// equivalent to Figure 1 from the paper:
// w = 64, k = 64, m = 128. The period of the LCG is 2^64. The XBG is xoroshiro128 version 1.0 [Blackman and Vigna 2018], which has a period of 2^128 - 1.
// The combining function is binary addition. The mixing function is a variant of the MurmurHash3 mixing function [Appleby 2011, 2016] identified by Doug
// Lea. The additive parameter a may be initialized to any odd integer, and the state variables s, x0, and x1 may be initialized to any values as long as
// x0 and x1 are not both zero. Because the periods of the subgenerators are relatively prime, the overall period of this LXM generator is
// 2^64 * (2^128 - 1) = 2^192 - 2^64.
using l64x128m = lxm_engine<lcg64_engine<>, xoroshiro128_engine, std::plus<std::uint_fast64_t>, lea64<std::uint_fast64_t>>;

// equivalent to Figure 2 from the paper:
// w = 64, k = 128, m = 256. It uses the same 64-bit mixing function but uses a different (256-bit) XBG, xoshiro256 [Blackman and Vigna 2018]. It also
// illustrates some interesting engineering tradeoffs when implementing a 128-bit LCG using 64-bit arithmetic. Computing the (128-bit) low half of two 128-bit
// operands requires computing the 128-bit product of the (64-bit) low halves, plus the (64-bit) low halves of products of two pairs of 64-bit values, each
// consisting of the high half one of 128-bit operand and the low half of the other. But testing seems to show that there is little extra benefit of using a
// 128-bit multiplier over a 65-bit multiplier; on the other hand, theory tells us that a 64-bit multiplier will produce an LCG of lower quality [Steele and
// Vigna 2021].Therefore we choose to use a multiplier of the form 264 + m where m < 264 and of course(m mod 8) = 5; this eliminates one 64-bit multiplication
// in the implementation. On the other hand, there is a benefit to be gained by using a full 128-bit additive parameter rather than settling for 64 bits. The
// code uses two uint_fast64_t values ah and al to represent the high and low halves of the additive parameter, and similarly uses two uint_fast64_t values sh
// and sl to represent the high and low halves of the LCG state.
using l128x256m = lxm_engine<lcg128_engine<>, xoshiro256_engine, std::plus<std::uint_fast64_t>, lea64<std::uint_fast64_t>>;

template<size_t N, typename SplittableRng> std::array<SplittableRng, N> split_n(SplittableRng& rand)
{
	auto make_array = [&rand]<size_t... Indices>(std::index_sequence<Indices...>) {
		auto split = [&rand](size_t) { return rand.split(); };

		// Order of calls to array{ ... } is left to right due to aggregate initialization rules.
		// Therefore, the calls to rand.split() is consistent between compilers.
		//
		//   "Initialize each element of the aggregate in the element order. That is, all value computations and side effects
		//   associated with a given element are sequenced before those of any element that follows it in order."
		//
		// source: https://en.cppreference.com/w/cpp/language/aggregate_initialization#Process
		return std::array<SplittableRng, N>{ split(Indices)... };
	};
	return make_array(std::make_index_sequence<N>{});
}

} // namespace lxm
