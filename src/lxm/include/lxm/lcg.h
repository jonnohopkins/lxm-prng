#pragma once

#include <cstdint>
#include <array>
#include <limits>
#include <utility>
#include <type_traits>
#include <ostream>
#include <istream>
#include <lxm/random.h>

// linear congruential pseudorandom number generators
//
// differs from std::linear_congruential_engine in the following ways:
//  - uses a fixed multiplier rather than multiplier and modulus template parameters
//  - uses a runtime additive parameter rather than an increment template parameter
//  - the additive parameter is always odd (regardless of seed).

namespace lxm {

namespace detail {
// multiply high unsigned
// returns the half high order bit result of (u * v) as an unsigned integer that has twice the size as its inputs
// source: Hacker's Delight 8-2: High-Order Half of 64-Bit Product (approx page 163)
// https://doc.lagout.org/security/Hackers%20Delight.pdf
template<typename UInt>
	requires std::is_integral_v<UInt> && std::is_unsigned_v<UInt>
constexpr UInt mulhu(UInt u, UInt v)
{
	const int  half_size = sizeof(UInt) * CHAR_BIT / 2;
	const UInt half_mask = (UInt(1) << half_size) - 1;
	UInt	   u0, v0, w0;
	UInt	   u1, v1, w1, w2, t;
	u0 = u & half_mask;
	u1 = u >> half_size;
	v0 = v & half_mask;
	v1 = v >> half_size;
	w0 = u0 * v0;
	t  = u1 * v0 + (w0 >> half_size);
	w1 = t & half_mask;
	w2 = t >> half_size;
	w1 = u0 * v1 + w1;
	return u1 * v1 + w2 + (w1 >> half_size);
}
} // namespace detail

template<std::uint_fast64_t FM = 0xde684fcac05b7c3aull> class lcg64_engine {
  public:
	using result_type = std::uint_fast64_t;

	static constexpr result_type default_seed	  = 0x16a72dbcf7859b44ull;
	static constexpr result_type fixed_multiplier = FM;

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	explicit lcg64_engine(result_type value = default_seed) noexcept { seed(value); }
	template<typename Sseq>
	explicit lcg64_engine(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, lcg64_engine>
	{
		seed(std::forward<Sseq>(sseq));
	}

	lcg64_engine(const lcg64_engine&)			 = default;
	lcg64_engine(lcg64_engine&&)				 = default;
	lcg64_engine& operator=(const lcg64_engine&) = default;
	lcg64_engine& operator=(lcg64_engine&&)		 = default;

	void seed(result_type value = default_seed) { seed(std::seed_seq{ value }); }
	template<typename Sseq>
	void seed(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, lcg64_engine>
	{
		std::array<std::uint_least32_t, 4> seeds;
		sseq.generate(seeds.begin(), seeds.end());
		a = detail::combine(seeds[0], seeds[1]) | 0b1;
		s = detail::combine(seeds[2], seeds[3]);
	}

	result_type operator()()
	{
		s = fixed_multiplier * s + a;
		return s;
	}

	void discard(unsigned long long z)
	{
		for (unsigned long long i = 0; i < z; ++i)
			operator()();
	}

	friend bool			 operator==(const lcg64_engine&, const lcg64_engine&) = default;
	friend std::ostream& operator<<(std::ostream& os, const lcg64_engine& rhs) { return os << rhs.a << ' ' << rhs.s; }
	friend std::istream& operator>>(std::istream& is, lcg64_engine& rhs)
	{
		is >> rhs.a;
		is >> rhs.s;
		return is;
	}

  private:
	result_type a; // additive parameter (must be odd)
	result_type s;
};

template<std::uint_fast64_t FML = 0x81dfd8f1fc8e94ecull> class lcg128_engine {
  public:
	using result_type = std::uint_fast64_t;

	static constexpr result_type default_seed		  = 0xaf1e9a92f88158f8ull;
	static constexpr result_type fixed_multiplier_low = FML; // Low half of fixed multiplier

	static constexpr result_type min() { return std::numeric_limits<result_type>::min(); }
	static constexpr result_type max() { return std::numeric_limits<result_type>::max(); }

	explicit lcg128_engine(result_type s = default_seed) noexcept { seed(s); }
	template<typename Sseq>
	explicit lcg128_engine(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, lcg128_engine>
	{
		seed(std::forward<Sseq>(sseq));
	}

	lcg128_engine(const lcg128_engine&)			   = default;
	lcg128_engine(lcg128_engine&&)				   = default;
	lcg128_engine& operator=(const lcg128_engine&) = default;
	lcg128_engine& operator=(lcg128_engine&&)	   = default;

	void seed(result_type s = default_seed) { seed(std::seed_seq{ s }); }
	template<typename Sseq>
	void seed(Sseq&& sseq)
		requires detail::seed_sequence<Sseq, lcg128_engine>
	{
		std::array<std::uint_least32_t, 8> seeds;
		sseq.generate(seeds.begin(), seeds.end());
		ah = detail::combine(seeds[0], seeds[1]);
		al = detail::combine(seeds[2], seeds[3]) | 0b1;
		sh = detail::combine(seeds[4], seeds[5]);
		sl = detail::combine(seeds[6], seeds[7]);
	}

	result_type operator()()
	{
		// The LCG is, in effect, "s = m * s + a" where m = ((1LL << 64) + fixed_multiplier_low)
		result_type u = fixed_multiplier_low * sl;
		sh			  = (fixed_multiplier_low * sh) + detail::mulhu(fixed_multiplier_low, sl) + sl + ah;
		sl			  = u + al;
		if (sl < u) // carry propagation
			++sh;
		return sh;
	}

	void discard(unsigned long long z)
	{
		for (unsigned long long i = 0; i < z; ++i)
			operator()();
	}

	friend bool			 operator==(const lcg128_engine&, const lcg128_engine&) = default;
	friend std::ostream& operator<<(std::ostream& os, const lcg128_engine& rhs) { return os << rhs.ah << ' ' << rhs.al << ' ' << rhs.sh << ' ' << rhs.sl; }
	friend std::istream& operator>>(std::istream& is, lcg128_engine& rhs)
	{
		is >> rhs.ah;
		is >> rhs.al;
		is >> rhs.sh;
		is >> rhs.sl;
		return is;
	}

  private:
	result_type ah, al; // additive high and low parameters (al must be odd)
	result_type sh, sl;
};

} // namespace lxm
