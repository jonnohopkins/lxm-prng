#pragma once

#include <type_traits>

namespace lxm {

// https://github.com/aappleby/smhasher/wiki/MurmurHash3
template<typename UInt32>
	requires(std::is_integral_v<UInt32> && std::is_unsigned_v<UInt32> && sizeof(UInt32) == 4)
struct murmur32 {
	constexpr UInt32 operator()(UInt32 value) const
	{
		value ^= (value >> 16);
		value *= 0x85ebca6bul;
		value ^= (value >> 13);
		value *= 0xc2b2ae35ul;
		return value ^ (value >> 16);
	}
};

// https://github.com/aappleby/smhasher/wiki/MurmurHash3
template<typename UInt64>
	requires(std::is_integral_v<UInt64> && std::is_unsigned_v<UInt64> && sizeof(UInt64) == 8)
struct murmur64 {
	constexpr UInt64 operator()(UInt64 value) const
	{
		value ^= (value >> 33);
		value *= 0xff51afd7ed558ccdull;
		value ^= (value >> 33);
		value *= 0xc4ceb9fe1a85ec53ull;
		return value ^ (value >> 33);
	}
};

// https://gist.github.com/degski/6e2069d6035ae04d5d6f64981c995ec2
template<typename UInt32>
	requires(std::is_integral_v<UInt32> && std::is_unsigned_v<UInt32> && sizeof(UInt32) == 4)
struct degski32 {
	constexpr UInt32 operator()(UInt32 value) const
	{
		value ^= (value >> 16);
		value *= 0x45d9f3bul;
		value ^= (value >> 16);
		value *= 0x45d9f3bul;
		return value ^ (value >> 16);
	}
};

// https://gist.github.com/degski/6e2069d6035ae04d5d6f64981c995ec2
template<typename UInt64>
	requires(std::is_integral_v<UInt64> && std::is_unsigned_v<UInt64> && sizeof(UInt64) == 8)
struct degski64 {
	constexpr UInt64 operator()(UInt64 value) const
	{
		value ^= (value >> 32);
		value *= 0xd6e8feb86659fd93ull;
		value ^= (value >> 32);
		value *= 0xd6e8feb86659fd93ull;
		return value ^ (value >> 32);
	}
};

// From Figure 1 of the paper: XLM Better splittable PRNGs
template<typename UInt64>
	requires(std::is_integral_v<UInt64> && std::is_unsigned_v<UInt64> && sizeof(UInt64) == 8)
struct lea64 {
	constexpr UInt64 operator()(UInt64 value) const
	{
		value = (value ^ (value >> 32)) * 0xdaba0b6eb09322e3ull;
		value = (value ^ (value >> 32)) * 0xdaba0b6eb09322e3ull;
		value = (value ^ (value >> 32));
		return value;
	}
};

// from the paper: a weaker mixer than murmur32. this is a "scrambler" and when created was not intended to be used as a mixer.
template<typename UInt>
	requires(std::is_integral_v<UInt> && std::is_unsigned_v<UInt>)
struct starstar {
	constexpr UInt operator()(UInt value) const { return std::rotl(value * 5, 7) * 9; }
};

} // namespace lxm
