#include <lxm/lxm.h>
#include <iostream>
#include <bitset>

// https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/util/random/package-summary.html#algorithms
using java_l64x128starstar =
	lxm::lxm_engine<lxm::lcg64_engine<0xd1342543de82ef95L>, lxm::xoroshiro128_engine, std::plus<std::uint_fast64_t>, lxm::starstar<std::uint_fast64_t>>;
using java_l64x128mix =
	lxm::lxm_engine<lxm::lcg64_engine<0xd1342543de82ef95L>, lxm::xoroshiro128_engine, std::plus<std::uint_fast64_t>, lxm::lea64<std::uint_fast64_t>>;
using java_l64x256mix =
	lxm::lxm_engine<lxm::lcg64_engine<0xd1342543de82ef95L>, lxm::xoshiro256_engine, std::plus<std::uint_fast64_t>, lxm::lea64<std::uint_fast64_t>>;
using java_l64x1024mix =
	lxm::lxm_engine<lxm::lcg64_engine<0xd1342543de82ef95L>, lxm::xoroshiro1024_engine, std::plus<std::uint_fast64_t>, lxm::lea64<std::uint_fast64_t>>;
using java_l128x128mix =
	lxm::lxm_engine<lxm::lcg128_engine<0xd1342543de82ef95L>, lxm::xoroshiro128_engine, std::plus<std::uint_fast64_t>, lxm::lea64<std::uint_fast64_t>>;
using java_l128x256mix =
	lxm::lxm_engine<lxm::lcg128_engine<0xd1342543de82ef95L>, lxm::xoshiro256_engine, std::plus<std::uint_fast64_t>, lxm::lea64<std::uint_fast64_t>>;
using java_l128x1024mix =
	lxm::lxm_engine<lxm::lcg128_engine<0xd1342543de82ef95L>, lxm::xoroshiro1024_engine, std::plus<std::uint_fast64_t>, lxm::lea64<std::uint_fast64_t>>;

int main()
{
	std::random_device rd;
	java_l64x128mix java_lxm_engine(rd());

	std::cout << "java: " << java_lxm_engine << "\n";
	for (auto i = 0; i < 100; ++i) {
		std::cout << java_lxm_engine() << '\n';
	}
	std::cout << '\n';
}
