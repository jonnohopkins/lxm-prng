package com.jonnohopkins;
import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

public class App 
{
    private static byte[] RandomGeneratorStateFromLongs(long[] state)
    {
        byte[] byteArray = new byte[state.length * 8];
        for(int i = 0; i < state.length; ++i) {
            for(int j = 0; j < 8; j++) {
                byte b = (byte)(state[i] >>> (j * 8));
                byteArray[i * 8 + (8 - j - 1)] = b;
            }
        }
        return byteArray;
    }

    public static void main( String[] args )
    {
        RandomGeneratorFactory<RandomGenerator> factory = RandomGeneratorFactory.of("L64X128MixRandom");
        long[] state = new long[] { Long.parseUnsignedLong("17020980989079035445"), Long.parseUnsignedLong("1860146862645846923"), Long.parseUnsignedLong("9029376777476953819"), Long.parseUnsignedLong("16768650356763846168") };

        byte[] byteArray = RandomGeneratorStateFromLongs(state);
        RandomGenerator r = factory.create(byteArray);
        for(int i = 0; i < 100; i++) {
            System.out.println(Long.toUnsignedString(r.nextLong()));
        }
    }
}
