## About

An implementation of a Splittable Pseudo-Random Number Generator from the paper [LXM: Better Splittable Pseudorandom Number Generators by Guy L. Steele Jr. and Sebastiano Vigna](doc/XLM%20Better%20splittable%20PRNGs%20-%20Guy%20L%20Steele%20Jr%20and%20Sebastiano%20Vigna%202021.pdf).

## Building

The project uses CMake as its build system. The tests depend on Google Test, which is included by CMake.

Select a `<dir>` to generate the project files and run:
 ```
cmake -B <dir>			# create the project
cmake --build <dir>		# build the project
cmake --open <dir>		# open the project
```
